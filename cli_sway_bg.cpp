/**
  * CLI application to set background/wallpaper on a system using swaywm.
  * Author: Sam St-Pettersen, 2023
  * Released under the MIT License
*/

#define FMT_HEADER_ONLY

#include <iostream>
#include <filesystem>
#include <fstream>
#include <vector>
#include <cstdlib>
#include <string>
#include <fmt/core.h>

using namespace std;

// Forward declarations for display methods.
int displayError(string, string);
int displayUsage(string, int);

int setBackground(string program, string bg, string cfg) {
    // Check that background file exists,
    // if not throw an error.
    if (!std::filesystem::exists(bg)) {
        return displayError(program, fmt::format
        ("Background file does not exist: \"{}\"", bg));
    }

    // Read in file and set background on applicable line.
    // Push each line to _out vector.
    vector<string> _out;
    ifstream ifile(cfg);
    string line;
    while (std::getline(ifile, line)) {
        if (line.find("output * bg") == 0) {
            line = fmt::format("output * bg \"{}\" fill", bg);
        }
        _out.push_back(line);
    }
    ifile.close();

    // Remove file.
    std::filesystem::remove(cfg);

    // Write file out line by line to replacement file.
    ofstream ofile(cfg, std::ios_base::app);
    for (string line: _out) {
        ofile << line << endl;
    }
    ofile.close();

    fmt::print("Set background to:\n\"{}\".\n", bg);
    cout << "Press Mod+Shift+c to refresh swaywm settings." << endl;

    return 0;
}

int displayError(string program, string err) {
    fmt::print("Error: {}.\n\n", err);
    return displayUsage(program, -1);
}

int displayUsage(string program, int exitCode) {
    cout << "CLI utility to set background/wallpaper on a system using swaywm." << endl;
    cout << "Written by Sam St-Pettersen <s.stpettersen@pm.me>\n" << endl;
    fmt::print("Usage: {} OPTION\n\n", program);
    cout << "Options:" << endl;
    cout << "bg=PATH_TO_BG_IMAGE       Specify the background/wallpaper to set.\n" << endl;
    return exitCode;
}

int main(int argc, char* argv[]) {
    int exitCode = 0;
    const string program = "cli_sway_bg";

    #ifdef _WIN32
        cout << "This program is only intended for Unix-like OSes." << endl;
        return exitCode;
    #endif

    string user(std::getenv("USER"));
    string cfg = fmt::format("/home/{}/.config/sway/config", user);

    if (!std::filesystem::exists(cfg)) {
        cout << "Error: swaywm not detected." << endl;
        cout << "Please install it and create your configuration" << endl;
        cout << "under ~/.config/.sway/\n" << endl;
        exitCode = displayUsage(program, -1);
        return exitCode;
    }

    // Create a symbolic link (~./swaycfg) to swaywm main
    // configuration file if it does not already exist.
    string link = fmt::format("/home/{}/swaycfg", user);

    #ifdef __unix__
        if (!std::filesystem::exists(link)) {
            std::filesystem::path target(cfg);
            std::filesystem::path lnk(link);

            std::error_code error;
            std::filesystem::create_symlink(target, lnk, error);
            if (error) {
                cout << "Error creating symlink:\n" << error.message() << "\n" << endl;
            }
            else {
                cout << "Created a symbolic link to swaywm main config file:" << endl;
                cout << "~/swaycfg\n" << endl;
            }
        }
    #endif

    if (argc > 1) {
        for (int i = 0; i < argc; i++) {
            string substr = "bg=";
            string a(argv[i]);
            if (a.find(substr) == 0) {
                size_t pos = a.find(substr);
                a.replace(pos, substr.length(), "");
                exitCode = setBackground(program, a, cfg);
                return exitCode;
            }
            else if (a.find(program) != 0 && i > 0) {
                exitCode = displayError(program,
                fmt::format("\"{}\" is not a valid option", a));
                return exitCode;
            }
        }
    }
    else {
        exitCode = displayUsage(program, 0);
    }

    return exitCode;
}
