TARGET=cli_sway_bg
SOURCE=cli_sway_bg.cpp
COMPILER=g++

make:
	$(COMPILER) -Ifmt-9.1.0/include $(SOURCE) -o $(TARGET)
	strip $(TARGET)

install:
	@echo Please run this as sudo/doas.
	upx -9 -o /usr/bin/$(TARGET) $(TARGET)

uninstall:
	@echo Please run this as sudo/doas.
	rm /usr/bin/$(TARGET)

clean:
	rm $(TARGET)
