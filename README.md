### cli\_sway\_bg
> Simple CLI application (C++ implementation) to set background when using [swaywm](https://swaywm.org).

##### How to use

> You will need to install a C++ compiler ([gcc/g++](http://gcc.gnu.org) recommended), [make](https://www.gnu.org/software/make) utility
and [UPX](https://upx.github.io) executable compressor to build from repository source.

* On [Void Linux](https://voidlinux.org), you can install the following packages with [XBPS](https://github.com/void-linux/xbps) (where gcc is GNU Compiler Collection):

<pre># xbps-install -S gcc make upx unzip</pre>

##### Build from source and install
First, extract [fmt](https://github.com/fmtlib/fmt) library files to current directory.

<pre>$ unzip fmt-9.1.0.zip</pre>

Then:

<pre>$ make</pre>
<pre># make install</pre>

##### Usage

<pre>$ cli_sway_bg</pre>

to create a symbolic link (~/swaycfg) to your Sway WM configuration.
This only needs to be done once.

<pre>$ cli_sway_bg bg=/path/to/wallpaper_or_background_image.jpg</pre>

Then press Mod+Shift+c to refresh Sway WM and, if valid, your specified wallpaper will now be set.

~~
